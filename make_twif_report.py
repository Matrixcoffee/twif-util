# stdlib
import argparse
import os.path
import time

# in-tree deps
import debug
from Report import Report


def parse_args():
	parser = argparse.ArgumentParser(description="compares two indexes and spits out information in a format suitable for easy inclusion in TWIF.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	parser.add_argument("old", help="Old index to compare.")
	parser.add_argument("new", help="New index to compare. Optional. If omitted, will use the latest, downloading it if necessary.", nargs='?')
	options = parser.parse_args()
	return options

def get_specified_index_or_latest(path_to_config, path_to_index=None):
	import Index

	if path_to_index is None:
		from Config import Config
		config = Config().load(path_to_config)
		return Index.get_latest_index(config)
	else:
		return Index.Index().load_from_file(path_to_index)

if __name__ == '__main__':
	opts = parse_args()
	debug.dprint(repr(opts))

	oldindex = get_specified_index_or_latest(None, opts.old)
	newindex = get_specified_index_or_latest("untracked/config.json", opts.new)

	report = Report(oldindex, newindex)

	bn = os.path.basename
	fmt = "%A, %B %-d %Y, Week %V"
	print("### Index difference report")
	print("Report generated on", time.strftime(fmt, time.gmtime()))
	print()
	print("Comparing differences between {} and {}".format(bn(oldindex.filename), bn(newindex.filename)))
	print()
	print("These are the apps that were updated, and are candidates for inclusion in TWIF. As usual, all new apps will automatically get a mention, but a mini-announcement that we can include is highly appreciated. As for the updated apps, there are way too many to include all of them, so please tell us which ones are important, and why.")
	print()

	removed = report.get_removed_apps()
	if removed:
		print("#### Removed Apps")
		print()
		print("".join(removed))
		print(len(removed), "apps were removed")
		print()

	added = report.get_added_apps()
	if added:
		print("#### Added Apps")
		print()
		print("".join(added))
		print(len(added), "apps were added")
		print()

	updated = report.get_updated_apps()
	if updated:
		print("#### Updated Apps")
		print()
		print("".join(updated))
		print(len(updated), "apps were upgraded")
		print()

	downgraded = report.get_downgraded_apps()
	if downgraded:
		print("#### !Downgraded Apps!")
		print()
		print("".join(downgraded))
		print(len(downgraded), "apps were downgraded (wUt?!)")
		print()

	beta_updated = report.get_beta_updated_apps()
	if beta_updated:
		print("#### Beta Updates")
		print()
		print("".join(beta_updated))
		print(len(beta_updated), "apps had beta upgrades")
		print()


