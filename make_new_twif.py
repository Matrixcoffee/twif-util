# stdlib
import argparse
import os.path
import re
import textwrap
import time

# in-tree deps
import debug
from Report import Report


def parse_args():
	parser = argparse.ArgumentParser(description="compares two indexes and spits out information in a format suitable for easy inclusion in TWIF.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	parser.add_argument("old", help="Old index to compare.")
	parser.add_argument("new", help="New index to compare. Optional. If omitted, will use the latest, downloading it if necessary.", nargs='?')
	options = parser.parse_args()
	return options

def get_specified_index_or_latest(path_to_config, path_to_index=None):
	import Index

	if path_to_index is None:
		from Config import Config
		config = Config().load(path_to_config)
		return Index.get_latest_index(config)
	else:
		return Index.Index().load_from_file(path_to_index)

def join(seq, sep, lastsep):
	""" Join strings in "seq" together using "sep" as separator.
	    BUT use "lastsep" for the last one. """

	if len(seq) == 0: return ""
	if len(seq) == 1: return seq[0]
	return lastsep.join((sep.join(seq[:-1]), seq[-1]))

def capitalize(phrase):
	if len(phrase) < 2: return phrase.capitalize()
	return phrase[0].upper() + phrase[1:]

def decapitalize(phrase):
	if len(phrase) < 2: return phrase.lower()
	if phrase[:2].isupper(): return phrase
	return phrase[0].lower() + phrase[1:]

def summarize(news):
	r = []

	header_pattern = re.compile("^#+ +(.+)$")
	first = True

	for line in news:
		line = line.rstrip()
		m = header_pattern.search(line)
		if not m: continue

		s = m.group(1)
		if first:	s = capitalize(s)
		else:		s = decapitalize(s)
		first = False
		r.append(s)

	return join(r, ", ", " and ")

if __name__ == '__main__':
	opts = parse_args()
	debug.dprint(repr(opts))

	oldindex = get_specified_index_or_latest(None, opts.old)
	newindex = get_specified_index_or_latest("untracked/config.json", opts.new)

	report = Report(oldindex, newindex)
	removed = report.get_removed_apps()
	added = report.get_added_apps()
	updated = report.get_updated_apps()
	downgraded = report.get_downgraded_apps()
	beta_updated = report.get_beta_updated_apps()

	with open("untracked/news.md", "r") as f:
		news = f.read().splitlines(True)

	summary = summarize(news) or "**FIXME**: write a summary"

	params = {
		"editionnumber": int((time.time() - 1523923200) / 604800),
		"title": "**FIXME**: set a title ",
		"summary": summary,
		"author": "Coffee",
		"date": time.strftime("%Y-%m-%d"),
		"number_of_added_apps": len(added),
		"number_of_removed_apps": len(removed),
		"number_of_updated_apps": len(updated),
		"number_of_beta_updated_apps": len(beta_updated),
		"number_of_downgraded_apps": len(downgraded)
	}

	frontmatter = textwrap.dedent("""\
	---
	title: "TWIF {editionnumber}: {title}"
	edition: {editionnumber}
	author: "{author}"
	number_of_added_apps: {number_of_added_apps}
	number_of_removed_apps: {number_of_removed_apps}
	number_of_updated_apps: {number_of_updated_apps}
	number_of_beta_updated_apps: {number_of_beta_updated_apps}
	number_of_downgraded_apps: {number_of_downgraded_apps}
	date: {date}
	---
	""").strip().format_map(params)

	print(frontmatter)
	print()

	header = textwrap.dedent("""\
	{{{{< twif_edition >}}}}

	In this edition: {summary}. {{{{<x>}}}}

	{{{{</x>}}}}There are **{{{{< getvalue number_of_added_apps >}}}}** new and **{{{{< getvalue number_of_updated_apps >}}}}** updated apps.

	<!--more-->{{{{<x>}}}} Dear translators. Please leave this line unchanged. Thank you. {{{{</x>}}}}

	{{{{< twif_preamble >}}}}
	""").strip().format_map(params)

	print(header)
	print()

	print("".join(news))

	if downgraded:
		print("### !Downgraded apps!")
		print()
		print("".join(downgraded))
		print("{{< getvalue number_of_downgraded_apps >}} apps were downgraded (wUt?!).")
		print()

	if added:
		print("### New apps")
		print()
		print("".join(added))

	if updated:
		print("### Updated apps")
		print()
		print("In total, **{{< getvalue number_of_updated_apps >}}** apps were updated this week. Here are the highlights:")
		print()
		print("".join(updated))

	if beta_updated:
		print("### Beta updates")
		print()
		print("""The following updates won't be automatically suggested to you unless you have "Unstable updates" enabled in the F-Droid app settings, but you can expand the "Versions" tab and install them manually. Note that these are marked beta for a reason: proceed at your own risk.""")
		print()
		print("".join(beta_updated))
		print("{{< getvalue number_of_beta_updated_apps >}} apps had beta upgrades.")
		print()

	if removed:
		print("### Removed apps")
		print()
		print("".join(removed))
		print("{{< getvalue number_of_removed_apps >}} apps were removed.")
		print()

	print("{{< twif_footer >}}")
