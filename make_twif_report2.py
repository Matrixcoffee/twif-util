# stdlib
import argparse
import os.path
import time

import debug

def parse_args():
	parser = argparse.ArgumentParser(description="Reads an index and spits out information in a format suitable for easy inclusion in TWIF. (Single index no-diff edition)")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	parser.add_argument("timestamp", type=int, help="Time stamp to use. Mandatory. Can generated from 'date -d <whenever> +%%s000'.")
	parser.add_argument("index", help="Index to use. Optional. If omitted, will use the latest, downloading it if necessary.", nargs='?')
	options = parser.parse_args()
	return options

class _Missing:
	@staticmethod
	def __repr__():
		return "!Missing!"

Missing = _Missing()

class Stats:
	def __init__(self):
		self.removed = 0
		self.added = 0
		self.upgraded = 0
		self.beta_upgraded = 0
		self.downgraded = 0
		self.total = 0

	def print_stats(self):
		print("Total items processed:", self.total)
		print("* Added:         {:3} ({:5.1%})".format(*self._p(self.added)))
		print("* Upgraded:      {:3} ({:5.1%})".format(*self._p(self.upgraded)))
		print("* Beta Upgraded: {:3} ({:5.1%})".format(*self._p(self.beta_upgraded)))
		print("* Downgraded!:   {:3} ({:5.1%})".format(*self._p(self.downgraded)))
		debug.dprint(repr(self.__dict__))

	def _p(self, value):
		return (value, value / self.total)

def format_app_for_twif(index, pkgid, archive=False):
	pkg = index.idx['apps'][pkgid]
	if archive:	return "**[{}](https://f-droid.org/wiki/page/{})**".format(pkg['name'], pkgid)
	else:		return "**[{}](https://f-droid.org/app/{})**".format(pkg['name'], pkgid)

def get_specified_index_or_latest(path_to_config, path_to_index=None):
	import Index

	if path_to_index is None:
		from Config import Config
		config = Config().load(path_to_config)
		return Index.get_latest_index(config)
	else:
		return Index.Index().load_from_file(path_to_index)


if __name__ == '__main__':
	opts = parse_args()
	debug.dprint(repr(opts))

	index = get_specified_index_or_latest("untracked/config.json", opts.index)

	apps = index.idx['apps']

	stats = Stats()
	bn = os.path.basename

	#timestamp = time.time() * 1000
	#timestamp = 1541030400000
	timestamp = opts.timestamp

	fmt = "%A, %B %-d %Y, Week %V"
	print("### Index difference report")
	print("Report generated on", time.strftime(fmt, time.gmtime()))
	print()
	print("Finding changes in {}".format(bn(index.filename)))
	print()
	print("Hi. You're reading an emergency report which was generated from a single index file. These are, as best as possible, the apps that were updated, and are candidates for inclusion in TWIF. As usual, all new apps will automatically get a mention, but a mini-announcement that we can include is highly appreciated. As for the updated apps, there are way too many to include all of them, so please tell us which ones are important, and why.")
	print()

	print("#### Added Apps")
	print()
	for appid, appdata in sorted(apps.items()):
		if appdata['added'] >= timestamp:
			try:
				description = ": " + appdata['summary'].strip().capitalize()
				if not description.endswith("."): description = description + "."
			except KeyError:
				description = ""
			print("* ", format_app_for_twif(index, appid), description, sep="")
			stats.added += 1
	print()
	print(stats.added, "apps were added")
	print()

	print("#### Updated Apps")
	print()
	for appid, appdata in sorted(apps.items()):
		stats.total += 1

		# no actual packages - skip
		if not appdata['packages']: continue

		# new package - already dealt with above
		if appdata['added'] >= timestamp: continue

		packages = appdata['packages']
		packages.sort(key=lambda x: x['versionCode'])

		oldpkgs = []
		newpkgs = []
		for pkg in packages:
			if pkg['added'] >= timestamp:
				newpkgs.append(pkg)
			else:
				oldpkgs.append(pkg)

		suggested_vc = int(appdata['suggestedVersionCode'])
		for suggested_pkg in packages:
			if suggested_vc == suggested_pkg['versionCode']: break
			suggested_pkg = None

		if suggested_pkg is None:
			suggested_pkg = packages[-1]

		if suggested_pkg['added'] >= timestamp and newpkgs:
			newver = suggested_pkg['versionName']
			appstr = format_app_for_twif(index, appid)
			if oldpkgs:
				oldver = oldpkgs[-1]['versionName']
				print("* {} was updated from {} to {}".format(appstr, oldver, newver))
			else:
				print("* {} was updated to {}".format(appstr, newver))
			stats.upgraded += 1

	print()
	print(stats.upgraded, "apps were upgraded")
	if stats.downgraded: print(stats.downgraded, "apps were downgraded (wUt?!)")
	print()

	print("#### Beta Updates")
	print()
	for appid, appdata in sorted(apps.items()):
		stats.total += 1

		# no actual packages - skip
		if not appdata['packages']: continue

		packages = appdata['packages']
		packages.sort(key=lambda x: x['versionCode'])

		oldpkgs = []
		newpkgs = []
		for pkg in packages:
			if pkg['added'] >= timestamp:
				newpkgs.append(pkg)
			else:
				oldpkgs.append(pkg)

		suggested_vc = int(appdata['suggestedVersionCode'])
		for suggested_pkg in packages:
			if suggested_vc == suggested_pkg['versionCode']: break
			suggested_pkg = None

		if suggested_pkg is None:
			suggested_pkg = packages[-1]

		if suggested_pkg is not packages[-1] and packages[-1]['added'] >= timestamp:
			newver = suggested_pkg['versionName']
			appstr = format_app_for_twif(index, appid)
			if oldpkgs:
				oldver = oldpkgs[-1]['versionName']
				print("* {} was updated from {} to {}".format(appstr, oldver, newver))
			else:
				print("* {} was updated to {}".format(appstr, newver))
			stats.upgraded += 1

	print()
	print(stats.beta_upgraded, "apps had beta upgrades")
	print()

	stats.print_stats()
